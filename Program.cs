﻿using System;

namespace task21
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 20;

            for (var i = 1; i < counter;)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");
                i++;
                i++;
            }
           Console.WriteLine(""); 
           Console.WriteLine("Press any key to exit");
           Console.ReadKey();
        }
    }
}
